class Machine {
    constructor(){
        this.isTurn = false;
    }

    turnOn(){
        this.isTurn = true;
        console.log('Вы включили ');
    };
    turnOff () {
        this.isTurn = false;
        console.log('Вы выключили ');
    }
}

class HomeAppliance extends Machine{
    constructor(){
        super();
        this.isPlug = false;
    }


    plugIn() {
        this.isPlug = true;
        console.log('Устройство включено в сеть')
    };
    plugOff (){
        this.isPlug = false;
        console.log('Устройство выключено из сети!')
    }
}

class WashingMachine extends HomeAppliance {

    run() {
        if(this.isPlug && this.isTurn) {
            console.log('Стиралка включилась')
        } else {
            console.log('Сначала поключите к сети и включите стиралку!')
        }
    };
}

class LightSource extends HomeAppliance{
    constructor() {
        super();
        this.lightLevel = 0;
    }

    setLevel(level) {
        if(level > 1 && level < 100) {
            this.lightLevel = level;
            console.log('Уровень освещенности ' + level);
        } else if (level < 1 || level > 100) {
            alert('Такого уровня нет');
        }
    };
}

class AutoVehicle extends Machine{
    constructor() {
        super();
        this.x = 0;
        this.y = 0;
    }

    setPosition(x, y) {
        this.x = x;
        this.y = y;
    }
};

class Car extends AutoVehicle{
    constructor() {
        super();
        this.speed = 10;
    }

    setSpeed(speed) {
        this.speed = speed;
    }

    run(x, y) {
        const interval = setInterval(() => {
            let newX = this.x + this.speed;
            if(newX >= x) {
                newX = x;
            }
            let newY = this.y + this.speed;
            if(newY >= y) {
                newY = y;
            }
            this.setPosition(newX, newY);
            console.log('Машина движется ' + newX + ' - ' + newY)

            if(newX === x && newY === y) {
                clearInterval(interval);
                console.log('Машина на месте')
            }
        }, 1000)
    }
}
